grammar Expr;

/*the grammar name and file name must match*/

@header
{
	package antlr;
}


//start symbol
prog: (decl | expr) + EOF		# Program
	;

decl: ID ':' INT_TYPE '=' NUM	# Declaration
	;

//antlr resolvers ambiguities in favor of the alternative given first
expr: expr '*' expr				# Multiplication
	| expr '+' expr				# Addition
	| ID						# Variable
	| NUM						# Number
	;

//tokens
ID : [a-z][a-zA-Z0-9_]*; //identifiers
NUM : '0' | '-'?[1-9][0-9]*;
INT_TYPE : 'INT';
COMMENT : '--' ~[\r\n]* -> skip;
WS : [ \t\n\r]+ -> skip;